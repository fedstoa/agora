An Agora is defined by its code of conduct; it is a contract that binds us within its confines. It is of utmost importance for it to be well specified and known by all.

- Be honest.
- When interacting with others through references or comments, please observe the [Principle of Charity](https://en.wikipedia.org/wiki/Principle_of_charity).
- Be constructive, rational, kind and respectful.
- Follow your own public set of values; your personal contract, which you may upload to the Agora as a file named ```contract.md``` to join it with this node.
- When you meet someone that you disagree with, please prioritize mutual understanding over winning an argument.
- If you made a mistake, apologize.
- If someone made a mistake and apologized thoroughly, please try to forgive them.

The members of an Agora can propose changes to its contract by reaching out to its [maintainer](https://flancia.org/me) or directly sending a [pull request](https://github.com/flancian/agora).

Thank you and have a nice day!